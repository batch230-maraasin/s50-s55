import { Card, Button } from 'react-bootstrap';
import {useState, useEffect} from 'react';
import PropTypes from 'prop-types';
import {Link} from "react-router-dom"

export default function CourseCard({courseProp}) {

  //console.log("Contents of props: ");
  //console.log(courseProp);
  //console.log(typeof props);

  const {_id, name, description, price} = courseProp;

  // State Hooks (useState) - a way to store information within a component and track this information
        // getter, setter
        //variable, function to change the value of a variable
  //const [count, setCount] = useState(0); // count =0;
  //const [seats, setSeats] = useState(30); // seats = 30;
  

  //function enroll(){
    /*if(seats > 0){
      setCount(count +1);     
      console.log('Enrollees: ' + count);
      setSeats(seats - 1);
      console.log('Seats: ' + seats);
      
    }
    else{
      alert("No more seats available.")           
    }
    */
    //setCount(count +1);     
    //console.log('Enrollees: ' + count);
    //setSeats(seats - 1);
    //console.log('Seats: ' + seats);
  //}

  // useEffect() always run the task on the initial render and/or every render (when the state changes in a component)
  // Initial render is when the component is run or displayed for the first time
  /*useEffect(()=>{
      if(seats === 0){
          alert('No more seats available');
      }
  }, [seats]);*/

  return (
      <Card>
          <Card.Body>
              <Card.Title>{name}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>{price}</Card.Text>
              <Button as={Link} to={`/courses/${_id}`}>Enroll</Button>
          </Card.Body>
      </Card>
  )
}

// Check if the CourseCard component is getting the correct property types
/*CourseCard.propTypes = {
    courseProp: PropTypes.shape({
      name: PropTypes.string.isRequired,
      description: PropTypes.string.isRequired,
      price: PropTypes.number.isRequired,
    })
}
*/
